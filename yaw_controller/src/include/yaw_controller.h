#ifndef yaw_controller
#define yaw_controller

#include "pid_control.h"

/*!*************************************************************************************
 *  \class     yaw_controller
 *
 *  \brief     Yaw Controller PID
 *
 *  \details   This class is in charge of control yaw angle using PID class.
 *
 *
 *  \authors   Pablo Santofimia Ruiz
 *
 *  \copyright Copyright 2016 Universidad Politecnica de Madrid (UPM)
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program. If not, see http://www.gnu.org/licenses/.
 ***************************************************************************************/

#include <ros/ros.h>

#include "xmlfilereader.h"

#include <math.h>

class YawController
{
public:

  void setUp();

  void start();

  void stop();

  void getOutput(float *dyaw);
  void setReference(float ref_yaw, float yaw_aux);
  void setFeedback(float yaw);

  //! Read Config
  bool readConfigs(std::string configFile);


  //! Constructor. \details Same arguments as the ros::init function.
  YawController();

  //! Destructor.
  ~YawController();

private:

  PID PID_Yaw2DYaw;

  //! Configuration file variable
  int idDrone;               // Id Drone integer (number of the drone)
  std::string yawconfigFile;    // Config File String name
  std::string stackPath;     // Config File String aerostack path name

  float error_yaw;

  // Controller Tuning Parameters

  float pid_yaw2dyaw_kp;
  float pid_yaw2dyaw_ki;
  float pid_yaw2dyaw_kd;
  bool pid_yaw2dyaw_enablesat;
  float pid_yaw2dyaw_satmax;
  float pid_yaw2dyaw_satmin;
  bool pid_yaw2dyaw_enableantiwp;
  float pid_yaw2dyaw_kw;

};
#endif
