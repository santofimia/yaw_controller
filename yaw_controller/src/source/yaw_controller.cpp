#include "yaw_controller.h"

//Constructor
YawController::YawController()
{

    std::cout << "Constructor: YawController" << std::endl;

}

//Destructor
YawController::~YawController() {}

bool YawController::readConfigs(std::string configFile)
{

    try
    {

    XMLFileReader my_xml_reader(configFile);



    /*********************************   Yaw Controller ( from Yaw to DYaw ) ************************************************/

    // Gain
    pid_yaw2dyaw_kp = my_xml_reader.readDoubleValue("Yaw_Controller:PID_Yaw2DYaw:Gain:Kp");
    pid_yaw2dyaw_ki = my_xml_reader.readDoubleValue("Yaw_Controller:PID_Yaw2DYaw:Gain:Ki");
    pid_yaw2dyaw_kd = my_xml_reader.readDoubleValue("Yaw_Controller:PID_Yaw2DYaw:Gain:Kd");
    pid_yaw2dyaw_enablesat = my_xml_reader.readDoubleValue("Yaw_Controller:PID_Yaw2DYaw:Saturation:enable_saturation");
    pid_yaw2dyaw_satmax = my_xml_reader.readDoubleValue("Yaw_Controller:PID_Yaw2DYaw:Saturation:SatMax");
    pid_yaw2dyaw_satmin = my_xml_reader.readDoubleValue("Yaw_Controller:PID_Yaw2DYaw:Saturation:SatMin");
    pid_yaw2dyaw_enableantiwp = my_xml_reader.readDoubleValue("Yaw_Controller:PID_Yaw2DYaw:Anti_wind_up:enable_anti_wind_up");
    pid_yaw2dyaw_kw = my_xml_reader.readDoubleValue("Yaw_Controller:PID_Yaw2DYaw:Anti_wind_up:Kw");


    }


    catch ( cvg_XMLFileReader_exception &e)
    {
        throw cvg_XMLFileReader_exception(std::string("[cvg_XMLFileReader_exception! caller_function: ") + BOOST_CURRENT_FUNCTION + e.what() + "]\n");
    }


    return true;

}

void YawController::setUp()
{

    ros::param::get("~stackPath", stackPath);
    if ( stackPath.length() == 0)
    {
        stackPath = "$(env AEROSTACK_STACK)";
    }
    ros::param::get("~droneId", idDrone);
    ros::param::get("~yaw_config_file", yawconfigFile);
    if ( yawconfigFile.length() == 0)
    {
        yawconfigFile="yaw_controller.xml";
    }

    bool readConfigsBool = readConfigs(stackPath+"/configs/drone"+cvg_int_to_string(idDrone)+"/"+yawconfigFile);

    if(!readConfigsBool)
    {
        std::cout << "Error init"<< std::endl;
        return;
    }

    std::cout << "Constructor: YawController...Exit" << std::endl;

}

void YawController::start()
{

    // Reset PID
    PID_Yaw2DYaw.reset();

}

void YawController::stop()
{

}

void YawController::setFeedback(float yaw ){

    PID_Yaw2DYaw.setFeedback(yaw);

}

void YawController::setReference(float ref_yaw, float yaw_aux){

    error_yaw = ref_yaw - yaw_aux;

    if (error_yaw > M_PI){
        ref_yaw -= 2 * M_PI;
    }
    else if(error_yaw < -M_PI){
        ref_yaw += 2 * M_PI;
    }

    PID_Yaw2DYaw.setReference(ref_yaw);

}

void YawController::getOutput(float *dyaw){

    /*********************************  Yaw Controller ( from Yaw to dYaw ) ************************************************/

    PID_Yaw2DYaw.setGains(pid_yaw2dyaw_kp,pid_yaw2dyaw_ki,pid_yaw2dyaw_kd);
    PID_Yaw2DYaw.enableMaxOutput(pid_yaw2dyaw_enablesat,pid_yaw2dyaw_satmin,pid_yaw2dyaw_satmax);
    PID_Yaw2DYaw.enableAntiWindup(pid_yaw2dyaw_enableantiwp,pid_yaw2dyaw_kw);

    *dyaw = -PID_Yaw2DYaw.getOutput();

}
